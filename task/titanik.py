import pandas as pd
import numpy as np


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    genders = ["Mr.", "Mrs.", "Miss."]
    result = []
    for gender in genders:
        summary = int(df[df["Name"].str.contains(gender)][["Age"]].isna().sum().iloc[-1])
        median = int(df[df["Name"].str.contains(gender)][["Age"]].median())
        result.append([gender,summary,median])
    return tuple(result)